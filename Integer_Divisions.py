"""
This is a learning game for kids to lear Integer Divisions.
The user will be given a problem and should answer it.
Provide a "Correct" or  "Incorrect" feedback,
handle bad input using Exceptions.
"""

import random
import re

digit = re.compile('\d+')
print('INTEGER DIVISIONS')

while True:
    # Find two integers that are evenly divisible
    a = random.randrange(1, 10)
    b = random.randrange(1, 10)
    while a % b != 0:
        a = random.randrange(1, 10)
        b = random.randrange(1, 10)
    try:
        q = '{}/{}='.format(a, b)
        s = input(q)
        if not digit.match(s):
            print('Please enter Integers Only!')
        elif int(s) == a / b:
            print('CORRECT!')
        else:
            print('INCORRECT')
    except:
        break
