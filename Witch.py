import tkinter
import random
import sys
import os


def buttonPressed(event):
    print("Pressed at [ " + str(event.x) + ", " + str(event.y) + " ]")
    randomizeCanvas()


def buttonReleased(event):
    print("Pressed at [ " + str(event.x) + ", " + str(event.y) + " ]")
    randomizeCanvas()


def randomColor():
    r1, r2, r3 = (random.randint(0, 255),
                  random.randint(0, 255),
                  random.randint(0, 255))
    return '#%02X%02X%02X' % (r1, r2, r3)


def randomizeCanvas():
    for i in range(8):
        for j in range(8):
            canvas.create_rectangle(i * width / 8,
                                    j * height / 8,
                                    (i + 1) * width / 8,
                                    (j + 1) * height / 8,
                                    fill=randomColor(), outline=randomColor())


base = tkinter.Tk()
base.title("Hostile Takeover")
base.overrideredirect(True)
width = base.winfo_screenwidth()
height = base.winfo_screenheight()
base.geometry("{0}x{1}+0+0".format(width, height))
canvas = tkinter.Canvas(base, bg=randomColor(), width=width, height=height)
canvas.pack()

# What the heck Professor
# I took all that create_rectangle junk out
randomizeCanvas()

canvas.bind("<Button-1>", buttonPressed)
canvas.bind("<ButtonRelease-1>", buttonReleased)

while True:
    try:
        base.mainloop()
    except:
        break

os.system("python %s" % sys.argv[0])
