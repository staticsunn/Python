'''
Search Engine Performance Optimization

'''

from datetime import datetime
import sys

if len(sys.argv) != 3:
    print('Usage: %s file keyword' % sys.argv[0])
    sys.exit(1)

with open(sys.argv[0], 'r') as f:
    lines = sorted(f.read().splitlines())

dt1 = datetime.now()

for line in lines:
    if line.find(sys.argv[1]) != -1:
        print(line)

dt2 = datetime.now()

f.close()

print("Execution time:", dt2.microsecond - dt1.microsecond)
