import random


class Elephant():
    hints = [
        'I have exceptional memory',
        'I am the largest land-living mammal in the world']

    # def __init__(self):
    def __str__(self):
        return random.choice(self.hints)


class Tiger():
    hints = [
        'I am the biggest cat',
        'I come in black and white or orange and black']

    # def __init__(self):
    def __str__(self):
        return random.choice(self.hints)


class Bat():
    hints = [
        'I use echo-location',
        'I can fly',
        'I see well in dark']

    # def __init__(self):
    def __str__(self):
        return random.choice(self.hints)


class Animal():

    def __init__(self, name):
        self.name = name

    def guess_who_am_i(self):
        hints = 0
        print('I will give you 3 hints, guess what animal I am')

        while hints < 3:
            if self.name.lower() == 'elephant':
                print(Elephant())
            elif self.name.lower() == 'tiger':
                print(Tiger())
            elif self.name.lower() == 'bat':
                print(Bat())
            hints += 1
            try:
                s = input('Who am I? ')
                if s == self.name:
                    print('You got it! I am %s' % self.name)
                    break
            except:
                break

        print('I\'m out of hints! The answer is: %s' % self.name)


e = Animal("elephant")
t = Animal("tiger")
b = Animal("bat")

e.guess_who_am_i()
t.guess_who_am_i()
b.guess_who_am_i()
