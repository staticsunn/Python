'''
Traverse (walk) "fortune1" folder,  you are interested in each file that is in
*txt or *log format, create a structure (tuple?) that will hold an absolute
path (use os.path.abspath(file) - see hints below) to each file and the text
from the file (don't complicate it, keep entire file content in 1 string), each
file tuple in a list, so your list of files will look like:

[("/path/to/my/file1", "Content of the file1"), ("/path/to/my/file2", "Content
of the file2")]
'''

import os
import sys
import pickle
import re

# Python complains about *txt and *log, .*(txt|log)$ works.
# sre_constants.error: nothing to repeat at position 0
txt_log_ext = re.compile('.*(txt|log)$', re.IGNORECASE)

p = '.'
r = []
if len(sys.argv) > 1:
    p = sys.argv[1]

for root, subdirs, files in os.walk(p):
    for f in files:
        path = os.path.abspath(f)
        if txt_log_ext.match(path):
            try:
                with open(path, 'rb') as f:
                        content = f.read()
                        r.append((path, content))
            except:
                continue

with open('raw_data.pickle', 'wb') as f:
    pickle.dump(r, f)
