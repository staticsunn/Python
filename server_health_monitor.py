import psutil
import datetime

boot_time = datetime.datetime.fromtimestamp(
    psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S")

html_output = "<!doctype html><html><head><meta charset=\"UTF-8\">" \
              "<title>psutil_homework1.py</title></head><body><table>"

html_output += "<tr><td>BOOT TIME</td><td>{}</td></tr>".format(boot_time)

cpu_util = psutil.cpu_percent(interval=1, percpu=True)
mem = psutil.virtual_memory()
THRESHOLD = 100 * 1024 * 1024  # 100 MiB

html_output += "<tr><td>CPU UTILIZATION</td><td><table>"

i = 1
for cpu in cpu_util:
    html_output += "<tr><td>CPU {}</td><td>{}%</td></tr>".format(i, cpu)
    i += 1

html_output += "</table></td></tr>"

html_output += "<tr><td>AVAILABLE MEMORY</td><td>{}</td></tr>".format(
    mem.available)
html_output += "<tr><td>USED MEMORY</td><td>{}</td></tr>".format(mem.used)
html_output += "<tr><td>USED PERCENTAGE</td><td>{}</td></tr>".format(
    mem.percent)

html_output += "</table></body></html>"

print(html_output)
