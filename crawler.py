from __future__ import absolute_import, print_function
import urllib.request
import sys
import re


# https://github.com/celery/celery/blob/master/examples/eventlet/webcrawler.py
# http://daringfireball.net/2009/11/liberal_regex_for_matching_urls
# My regex was something like:
# url_regex = re.compile('href=[\'"]?([^ ]+)['"]?')
url_regex = re.compile(
    r'\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))',
    re.IGNORECASE)
title_regex = re.compile('<title>(.*)</title>', re.IGNORECASE)


def crawl(url):
    try:
        u = urllib.request.urlopen(url)
    except:
        return
    c = u.read()
    if len(c) <= 0:
        return

    title = title_regex.search(str(c))
    if title is None:
        title = 'Unknown Title!'

    for m in url_regex.finditer(str(c)):
        print(m.group(1), end='')
        print(': ' + title.group(1))
        crawl(m.group(1))

if len(sys.argv) != 2:
    print('Usage: %s <url>' % sys.argv[0])
    sys.exit(1)

crawl(sys.argv[1])
